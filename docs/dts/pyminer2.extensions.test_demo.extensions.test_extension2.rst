pyminer2.extensions.test\_demo.extensions.test\_extension2 package
==================================================================

Submodules
----------

pyminer2.extensions.test\_demo.extensions.test\_extension2.entry module
-----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.entry
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.interface module
---------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.interface
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.menu module
----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.menu
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension2.subwindow module
---------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2.subwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension2
   :members:
   :undoc-members:
   :show-inheritance:
