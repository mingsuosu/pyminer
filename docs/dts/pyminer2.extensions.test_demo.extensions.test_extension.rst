pyminer2.extensions.test\_demo.extensions.test\_extension package
=================================================================

Submodules
----------

pyminer2.extensions.test\_demo.extensions.test\_extension.entry module
----------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.entry
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.interface module
--------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.interface
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.menu module
---------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.menu
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.extensions.test\_extension.subwindow module
--------------------------------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension.subwindow
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo.extensions.test_extension
   :members:
   :undoc-members:
   :show-inheritance:
