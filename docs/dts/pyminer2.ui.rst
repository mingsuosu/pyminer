pyminer2.ui package
===================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.ui.base
   pyminer2.ui.common
   pyminer2.ui.generalwidgets
   pyminer2.ui.pmwidgets
   pyminer2.ui.source

Submodules
----------

pyminer2.ui.pyqtsource\_rc module
---------------------------------

.. automodule:: pyminer2.ui.pyqtsource_rc
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui
   :members:
   :undoc-members:
   :show-inheritance:
