pyminer2.ui.generalwidgets.browser package
==========================================

Submodules
----------

pyminer2.ui.generalwidgets.browser.browser module
-------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.browser.browser
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.browser
   :members:
   :undoc-members:
   :show-inheritance:
