pyminer2.workspace.datamanager package
======================================

Submodules
----------

pyminer2.workspace.datamanager.converter module
-----------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.converter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.datamanager module
-------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.datamanager
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.dataset module
---------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.dataset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.exceptions module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.historyset module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.historyset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.metadataset module
-------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.metadataset
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.recyclebin module
------------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.recyclebin
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.variable module
----------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.variable
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.workspace.datamanager.varset module
--------------------------------------------

.. automodule:: pyminer2.workspace.datamanager.varset
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.workspace.datamanager
   :members:
   :undoc-members:
   :show-inheritance:
