Introduction
============

Brief
--------
PyMiner一款基于数据工作空间的数学工具，通过加载插件的方式实现不同的需求，用易于操作的形式完成数学计算相关工作。官方网站：www.py2cn.com

License
-------
本项目遵循GPL许可证。此外，对个人用户来说，本项目支持自由修改源码和使用，但是不支持任何未经授权许可的商用或其他盈利性行为，也不支持任何未经授权申请著作权的行为。如有违反以上许可，本项目管理团队有权进行否决。 许可解释权归属 PyMiner Development Team。

Cut
--------

预览1(主界面)
>>>>>>>>>>

.. image:: /image/show1.jpg
    :scale: 40%

预览2(表格显示)
>>>>>>>>>>

.. image:: /image/show2.jpg
    :scale: 40%

预览3(编辑器)
>>>>>>>>>>

.. image:: /image/show3.png
    :scale: 40%